# up_service

A new Flutter project.

To run locally
Make sure java is installed
Get the key.jks from a project admin
copy the file to a location you'll easily reference

Create a key.properties file in the android folder with content that match this format
storePassword=<Ask for this>
keyPassword=<Ask for this>
keyAlias=najibu_key
storeFile=<absolute location of the keyfile you got>
Create a Google Auth SHA
the default password of the following is android
keytool -list -v -alias androiddebugkey -keystore ~/.android/debug.keystore
Ref: https://developers.google.com/android/guides/client-auth

Copy the SHA1 and SHA256 to firebase settings
This is required for the Log in to work.
